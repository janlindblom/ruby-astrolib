require "spec_helper"

RSpec.describe AstroLib::Astro do
  it "has TWO_PI" do
    expect(AstroLib::Astro::TWO_PI).to be 2 * Math::PI
    expect(AstroLib::Astro::TWO_PI).to be_a Float
  end
end
