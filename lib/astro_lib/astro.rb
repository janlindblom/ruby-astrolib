module AstroLib
  module Astro

    # Two times Pi
    TWO_PI = 2.0 * Math::PI

    # Pi divided by two
    PI_OVER_TWO = Math::PI / 2.0

    # Two divided by Pi
    TWO_OVER_PI = 2.0 / Math::PI

    # Degrees in a circle = 360
    DEG_PER_CIRCLE = 360.0

    # Arc minutes in one degree = 60
    MINUTES_PER_DEGREE = 60.0

    # Arc seconds in one degree = 3600
    SECONDS_PER_DEGREE = 60.0 * MINUTES_PER_DEGREE

    # Julian millenia conversion constant = 100 * days per year
    TO_CENTURIES = 36525.0

    # Hours in one day as a Float
    HOURS_PER_DAY = 24.0

    # Hours in one day as an integer
    IHOURS_PER_DAY = HOURS_PER_DAY.to_i

    # The fraction of one day equivalent to one hour = 1/24.
    DAYS_PER_HOUR = 1 / HOURS_PER_DAY

    # Minutes in one hour as a Float
    MINUTES_PER_HOUR = 60.0

    # Minutes in one hour as an integer
    IMINUTES_PER_HOUR = MINUTES_PER_HOUR.to_i

    # Seconds in one minute
    SECONDS_PER_MINUTE = 60

    # Seconds in one hour
    SECONDS_PER_HOUR = IMINUTES_PER_HOUR * SECONDS_PER_MINUTE

    # Seconds in one day
    SECONDS_PER_DAY = IHOURS_PER_DAY * SECONDS_PER_HOUR

    # Milliseconds in one second
    MILLISECONDS_PER_SECOND = 1000

    # Milliseconds in one hour
    MILLISECONDS_PER_HOUR = MILLISECONDS_PER_SECOND * SECONDS_PER_HOUR

    # Our default epoch.
    # The Julian Day which represents noon on 2001-01-01.
    J2000 = 2451545.0

    # Our generic "invalid" double value.
    INVALID = -1.0

    # A convenient rounding value
    ROUND_UP = 0.5
  end
end