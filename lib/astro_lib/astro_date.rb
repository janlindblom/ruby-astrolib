require "astro_lib/astro"

module AstroLib
  class AstroDate

    # day of the month
    attr_accessor :day

    # month of the year
    attr_accessor :month

    # year
    attr_accessor :year

    # Seconds past midnight == day fraction.
    # Valid values range from 0 to Astro::SECONDS_PER_DAY-1.
    attr_accessor :second

    def initialize(options=nil)

    end
  end
end